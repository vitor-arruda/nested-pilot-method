#pragma once
#include "./valleys.h"

/**
 * METRIC definitions depending on the heuristic set at compile time
 *
 */

#ifdef USE_HEURISTIC_H0
// HSA fitness (H0)
#define METRIC(item, plate, min_width, min_height)                       \
    {                                                                    \
        item.width == plate.width &&item.height == plate.taller_wall,    \
            item.width == plate.width &&item.height > plate.taller_wall, \
            item.width == plate.width &&item.height < plate.taller_wall, \
            item.width < plate.width &&item.height == plate.taller_wall, \
            item.width *item.height                                      \
    }
#endif

#ifdef USE_HEURISTIC_H1
// H1
#define METRIC(item, plate, min_width, min_height)                                                                                      \
    {                                                                                                                                   \
        ((item.width == plate.width) + (item.height == plate.left_wall) + (item.height == plate.right_wall)),                           \
            (item.width == plate.width && (item.height == plate.left_wall || item.height == plate.right_wall)) * item.height,           \
            (item.width == plate.width || item.height == plate.left_wall || item.height == plate.right_wall) * item.width *item.height, \
            item.width *item.height,                                                                                                    \
            0                                                                                                                           \
    }
#endif

#ifdef USE_HEURISTIC_H2
// H2
#define METRIC(item, plate, min_width, min_height)                                                                                                                                                             \
    {                                                                                                                                                                                                          \
        -(plate.top_gap - item.height < min_height) * item.width *(plate.top_gap - item.height) - (plate.width - item.width < min_width) * MIN(plate.valley_height, item.height) * (plate.width - item.width), \
            ((item.width == plate.width) + (item.height == plate.left_wall) + (item.height == plate.right_wall)),                                                                                              \
            (item.width == plate.width && (item.height == plate.left_wall || item.height == plate.right_wall)) * item.height,                                                                                  \
            (item.width == plate.width || item.height == plate.left_wall || item.height == plate.right_wall) * item.width *item.height,                                                                        \
            item.width *item.height                                                                                                                                                                            \
    }
#endif

#ifndef METRIC
// define MISSING_METRIC to alert the user that the code needs the METRIC definition on compile time
#define MISSING_METRIC
#define METRIC(item, plate, min_width, min_height) \
    {                                              \
        -1, -1, -1, -1, -1                         \
    }
#endif

namespace NestedPilot
{
    using namespace Valleys;

    // to be read by client code
    int64_t g_child_expansion_number = -1;
    int64_t g_child_total_number = -1;
    int64_t g_depth = -1;

    // to be set by client code
    clock_t g_start_checkpoint;

    // vector used inside `heuristic_pack` (could have been a local variable)
    std::vector<std::tuple<UnpackInformation, int64_t, Item>> pack_trace;

    /**
     * @brief Pack the remaining items using the heuristic. Stop early if waste bounds are exceeded.
     *
     * @param plates skyline structure (see valleys.h)
     * @param heap max-heap of valleys (see valleys.h)
     * @param container_size
     * @param available_items (vector of Item structs, see valleys.h)
     * @param current_area Area sum of the items already packed at the moment this function is called
     * @param current_best_area Best solution (area) found in the search until the moment.
     * @param sibling_best Best solution (area) found in a sibling node of the node that called this function (
     *                     in the NestedPilot search tree). Used for early stopping if this function detects that
     *                     it cannot do better than `sibling_best`
     * @param current_waste Accumulated waste areas due to valleys flattened before this function was called
     * @param current_solution Solution struct (see valleys.h) where packed items are recorded
     * @param solution_out Solution struct (see valleys.h) that is only updated when a new best solution is found
     * @param items_increasing_height Vector of Item structs (see valleys.h) sorted in increasing height (necessary for
     *                                H2 heuristic)
     * @param smallest_width_index Index (in the `plates` vector) of the still-unpacked item with the smallest width
     * @param smallest_height_index Index (in the `plates` vector) of the still-unpacked item with the smallest height
     * @return int64_t
     */
    int64_t heuristic_pack(std::vector<Plate> &plates,
                           RandomHeap<int64_t> &heap,
                           std::pair<int64_t, int64_t> container_size,
                           std::vector<Item> &available_items,
                           int64_t current_area,
                           int64_t &current_best_area,
                           int64_t sibling_best,
                           int64_t current_waste,
                           Solution &current_solution,
                           Solution &solution_out,
                           std::vector<Item> &items_increasing_height,
                           int64_t smallest_width_index,
                           int64_t smallest_height_index)
    {

        pack_trace.clear();

        // loop packing items. "break" stops the loop
        while (true)
        {

            // if cannot do better than a sibling, give up
            if (current_waste > container_size.first * container_size.second - sibling_best)
            {
                current_area = -1;
                break;
            }

            auto plate = min_valley(plates, heap);

            // if the container is full, finish (update the best solution record, if we found an even better solution)
            if (plate.is_final_valley())
            {
                if (current_area > current_best_area)
                {
                    clock_t checkpoint = std::clock();
                    current_best_area = current_area;
                    solution_out = current_solution;
                    std::cout << "found better solution when expanding child number  "
                              << g_child_expansion_number
                              << "/"
                              << g_child_total_number
                              << " at depth "
                              << g_depth
                              << " at time "
                              << (((double)checkpoint - g_start_checkpoint) / (double)CLOCKS_PER_SEC)
                              << " seconds"
                              << std::endl;
                    std::cout << "found greater area: " << current_best_area << std::endl;
                    std::cout << "waste: " << (container_size.first * container_size.second - current_best_area) << std::endl;
                    solution_out.print();
                }
                break;
            }

            Item best_item = FALSE_ITEM_GEOMETRY_DEFAULT;
            int64_t best_item_index = FALSE_ITEM_INDEX;
            std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t> best_item_metric;

            int64_t smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;

            int64_t smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;

            // decide best item to pack. Calculate the fitness score for each one and pack the max-scoring one
            for (int64_t item_index = 0; item_index < available_items.size() && available_items[item_index].width <= plate.width; item_index++)
            {
                auto item = available_items[item_index];

                if (item.count == 0 || plate.height + item.height > container_size.second)
                {
                    continue;
                }

                bool matched_width = false;
                bool matched_height = false;

                int64_t smallest_width_index_backup = smallest_width_index;
                int64_t smallest_height_index_backup = smallest_height_index;

                if (item.count == 1 && smallest_width_index == item_index)
                {
                    matched_width = true;
                    while (++smallest_width_index < available_items.size() && available_items[smallest_width_index].count == 0)
                    {
                    }

                    smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;
                }

                if (item.count == 1 && items_increasing_height[smallest_height_index].count == item_index) // .count hack
                {
                    matched_height = true;
                    while (++smallest_height_index < items_increasing_height.size() && available_items[items_increasing_height[smallest_height_index].count].count == 0)
                    {
                    }

                    smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;
                }

                std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t> item_metric = METRIC(item, plate, smallest_width, smallest_height);

                if (matched_width)
                {
                    smallest_width_index = smallest_width_index_backup;
                    smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;
                }

                if (matched_height)
                {
                    smallest_height_index = smallest_height_index_backup;
                    smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;
                }

                if (best_item_index == FALSE_ITEM_INDEX || item_metric > best_item_metric)
                {
                    best_item_metric = item_metric;
                    best_item_index = item_index;
                    best_item = available_items[item_index];
                    continue;
                }
            } // end for

            if (best_item.count == 1 && smallest_width_index == best_item_index)
            {
                while (++smallest_width_index < available_items.size() &&
                       available_items[smallest_width_index].count == 0)
                {
                }

                smallest_width = smallest_width_index < available_items.size()
                                     ? available_items[smallest_width_index].width
                                     : container_size.first;
            }

            if (best_item.count == 1 && items_increasing_height[smallest_height_index].count == best_item_index) // .count hack
            {
                while (++smallest_height_index < items_increasing_height.size() &&
                       available_items[items_increasing_height[smallest_height_index].count].count == 0)
                {
                }

                smallest_height = smallest_height_index < items_increasing_height.size()
                                      ? items_increasing_height[smallest_height_index].height
                                      : container_size.second;
            }

            auto unpack_info = pack_one(plates, heap, container_size, best_item, plate.x0);
            if (IS_REAL_ITEM(best_item_index))
            {
                available_items[best_item_index].count--;
                current_solution.append({.item_width = best_item.width,
                                         .item_height = best_item.height,
                                         .x = unpack_info.packed_x,
                                         .y = plate.height});
            }

            pack_trace.push_back({unpack_info, best_item_index, best_item});

            current_area += best_item.width * best_item.height;
            current_waste += unpack_info.local_waste;
        }

        // unpack all items that we packed in the loop above
        for (int64_t i = pack_trace.size() - 1; i >= 0; i--)
        {
            auto [unpack_info, item_index, item] = pack_trace[i];

            unpack_one(plates, heap, container_size, item, unpack_info);
            if (IS_REAL_ITEM(item_index))
            {
                available_items[item_index].count++;
                current_solution.pop();
            }
        }

        return current_area;
    }

    /**
     * @brief
     *
     * @param plates skyline structure (see valleys.h).
     * @param heap max-heap of valleys (see valleys.h).
     * @param container_size
     * @param available_items vector of Item structs (see valleys.h).
     * @param current_area Area sum of the items already packed at the moment this function is called.
     * @param current_best_area Best solution (area) found in the search until the moment.
     * @param current_waste Accumulated waste areas due to valleys flattened before this function was called.
     * @param sibling_best Best solution (area) found in a sibling node of the node that called this function (
     *                     in the NestedPilot search tree). Used for early stopping if this function detects that
     *                     it cannot do better than `sibling_best`.
     * @param nesting_level Nesting levels remaining for this function invokation (if 0, we immediately call `heuristic_pack`).
     * @param current_solution Solution struct (see valleys.h) where packed items are recorded.
     * @param solution_out Solution struct (see valleys.h) that is only updated when a new best solution is found.
     * @param waste_lower_bound Difference between container area and sum of all item areas. Used to detect if we reached
     *                          an optimal solution.
     * @param last_best Non-null only when NestedPilot is called on the root node (empty container).
     *                  This is a list of the solution areas achieved with each item in the previous invocation of NestedPilot
     *                  on the root node. The list is used for "Memoization" and "Row 2 Permutation",
     *                  as denomimated in the thesis.
     * @param items_increasing_height Vector of Item structs (see valleys.h) sorted in increasing height (necessary for
     *                                H2 heuristic).
     * @param smallest_width_index Index (in the `plates` vector) of the still-unpacked item with the smallest width.
     * @param smallest_height_index Index (in the `plates` vector) of the still-unpacked item with the smallest height.
     * @param is_top_level true when this invocation has the highest nesting level
     * @return int64_t
     */
    int64_t NestedPilot(std::vector<Plate> &plates,
                        RandomHeap<int64_t> &heap,
                        std::pair<int64_t, int64_t> container_size,
                        std::vector<Item> &available_items,
                        int64_t current_area,
                        int64_t &current_best_area,
                        int64_t current_waste,
                        int64_t sibling_best,
                        int64_t nesting_level,
                        Solution &current_solution,
                        Solution &solution_out,
                        int64_t waste_lower_bound,
                        std::vector<std::pair<int64_t, std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t>>> *last_best,
                        std::vector<Item> &items_increasing_height,
                        int64_t smallest_width_index,
                        int64_t smallest_height_index,
                        bool is_top_level = false)
    {

        if (current_waste > (container_size.first * container_size.second - sibling_best))
        {
            return -1;
        }

        if (nesting_level == 0)
        {
            return heuristic_pack(
                plates,
                heap,
                container_size,
                available_items,
                current_area,
                current_best_area,
                current_waste,
                sibling_best,
                current_solution,
                solution_out,
                items_increasing_height,
                smallest_width_index,
                smallest_height_index);
        }
        else
        {
            if (is_top_level)
            {
                g_depth = current_solution.packings.size();
            }

            auto plate = min_valley(plates, heap);

            if (plate.is_final_valley())
            {
                if (current_area > current_best_area)
                {
                    clock_t checkpoint = std::clock();
                    current_best_area = current_area;
                    solution_out = current_solution;
                    std::cout << "found better solution when expanding child number  "
                              << g_child_expansion_number
                              << "/"
                              << g_child_total_number
                              << " at depth "
                              << g_depth
                              << " at time "
                              << (((double)checkpoint - g_start_checkpoint) / (double)CLOCKS_PER_SEC)
                              << " seconds"
                              << std::endl;
                    std::cout << "found greater area: " << current_best_area << std::endl;
                    std::cout << "waste: " << (container_size.first * container_size.second - current_best_area) << std::endl;
                    solution_out.print();
                }

                return current_area;
            }

            Item best_item;
            int64_t best_area = -2;
            int64_t best_item_index = FALSE_ITEM_INDEX;
            int64_t best_plate_x0 = plate.x0;

            // vector of <item index, item metric> tuples (where item metric is itself a tuple)
            std::vector<std::pair<int64_t, std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t>>> best_items;

            int64_t smallest_width = smallest_width_index < available_items.size()
                                         ? available_items[smallest_width_index].width
                                         : container_size.first;

            int64_t smallest_height = smallest_height_index < items_increasing_height.size()
                                          ? items_increasing_height[smallest_height_index].height
                                          : container_size.second;

            if (last_best && last_best->size() > 0)
            {
                // don't bring 1st element because we already know its updated value from last time
                best_items = std::vector<std::pair<int64_t, std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t>>>(last_best->begin() + 1, last_best->end());
                last_best->resize(1);

                best_item_index = (*last_best)[0].first;
                if (best_item_index < 0)
                {
                    std::cout << "ERROR" << std::endl;
                    exit(1);
                }
                best_item = available_items[best_item_index];
                best_area = std::get<0>((*last_best)[0].second);
                best_plate_x0 = plate.x0;

                sibling_best = best_area;
            }
            else
            {
                // sort the items based on their metric scores
                for (int64_t item_index = 0; item_index < available_items.size() && available_items[item_index].width <= plate.width; item_index++)
                {
                    auto item = available_items[item_index];

                    if (item.count == 0 || plate.height + item.height > container_size.second)
                    {
                        continue;
                    }

                    bool matched_width = false;
                    bool matched_height = false;

                    int64_t smallest_width_index_backup = smallest_width_index;
                    int64_t smallest_height_index_backup = smallest_height_index;

                    if (item.count == 1 && smallest_width_index == item_index)
                    {
                        matched_width = true;
                        while (++smallest_width_index < available_items.size() &&
                               available_items[smallest_width_index].count == 0)
                        {
                        }

                        smallest_width = smallest_width_index < available_items.size()
                                             ? available_items[smallest_width_index].width
                                             : container_size.first;
                    }

                    if (item.count == 1 && items_increasing_height[smallest_height_index].count == item_index) // .count hack
                    {
                        matched_height = true;
                        while (++smallest_height_index < items_increasing_height.size() &&
                               available_items[items_increasing_height[smallest_height_index].count].count == 0)
                        {
                        }

                        smallest_height = smallest_height_index < items_increasing_height.size()
                                              ? items_increasing_height[smallest_height_index].height
                                              : container_size.second;
                    }

                    best_items.push_back({item_index, METRIC(item, plate, smallest_width, smallest_height)});

                    if (matched_width)
                    {
                        smallest_width_index = smallest_width_index_backup;
                        smallest_width = smallest_width_index < available_items.size()
                                             ? available_items[smallest_width_index].width
                                             : container_size.first;
                    }

                    if (matched_height)
                    {
                        smallest_height_index = smallest_height_index_backup;
                        smallest_height = smallest_height_index < items_increasing_height.size()
                                              ? items_increasing_height[smallest_height_index].height
                                              : container_size.second;
                    }
                }

                std::sort(best_items.begin(), best_items.end(), [](auto a, auto b)
                          { return a.second > b.second; });

                {
                    Item false_item = {.width = plate.width, .height = plate.valley_height};

                    best_items.push_back({FALSE_ITEM_INDEX, METRIC(false_item, plate, smallest_width, smallest_height)});
                }
            }

            // breadth limiting
            int64_t size = (best_items.size() + 3) / 4;
            if (size < 10)
            {
                size = MIN(best_items.size(), 10);
            }
            if (size > 150)
            {
                size = 150;
            }
            best_items.resize(size);

            if (is_top_level)
            {
                g_child_total_number = best_items.size();
            }

            int64_t child_expansion_number = 0;
            for (auto [item_index, item_metric] : best_items)
            {

                child_expansion_number++;
                if (is_top_level)
                {
                    g_child_expansion_number = child_expansion_number;
                }
                if (last_best && std::get<0>(item_metric) == FALSE_ITEM_INDEX)
                {
                    last_best->push_back({item_index, {0, 0, 0, 0, 0}});
                    continue;
                }

                Item item = IS_REAL_ITEM(item_index)
                                ? available_items[item_index]
                                : Item(FALSE_ITEM_GEOMETRY_DEFAULT);

                auto unpack_info = pack_one(plates, heap, container_size, item, plate.x0);
                if (IS_REAL_ITEM(item_index))
                {
                    available_items[item_index].count--;
                    current_solution.append({.item_width = item.width,
                                             .item_height = item.height,
                                             .x = unpack_info.packed_x,
                                             .y = plate.height});
                }

                bool matched_width = false;
                bool matched_height = false;
                int64_t smallest_width_index_backup = smallest_width_index;
                int64_t smallest_height_index_backup = smallest_height_index;

                if (item.count == 1 && smallest_width_index == item_index)
                {
                    matched_width = true;
                    while (++smallest_width_index < available_items.size() && available_items[smallest_width_index].count == 0)
                    {
                    }

                    smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;
                }

                if (item.count == 1 && items_increasing_height[smallest_height_index].count == item_index) // .count hack
                {
                    matched_height = true;

                    while (++smallest_height_index < items_increasing_height.size() && available_items[items_increasing_height[smallest_height_index].count].count == 0)
                    {
                    }

                    smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;
                }

                int64_t area = NestedPilot(plates,
                                           heap,
                                           container_size,
                                           available_items,
                                           current_area + item.width * item.height,
                                           current_best_area,
                                           current_waste + unpack_info.local_waste,
                                           best_area,
                                           nesting_level - 1,
                                           current_solution,
                                           solution_out,
                                           waste_lower_bound,
                                           nullptr,
                                           items_increasing_height,
                                           smallest_width_index,
                                           smallest_height_index);

                if (matched_width)
                {
                    smallest_width_index = smallest_width_index_backup;
                    smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;
                }

                if (matched_height)
                {
                    smallest_height_index = smallest_height_index_backup;
                    smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;
                }

                unpack_one(plates, heap, container_size, item, unpack_info);
                if (IS_REAL_ITEM(item_index))
                {
                    available_items[item_index].count++;
                    current_solution.pop();
                }

                if (last_best)
                {
                    last_best->push_back({item_index, {area, 0, 0, 0, 0}});

                    if (area > best_area)
                    {
                        std::swap((*last_best)[0], (*last_best)[last_best->size() - 1]);
                    }
                }

                if (area > best_area)
                {
                    best_item = item;
                    best_area = area;
                    best_item_index = item_index;
                    best_plate_x0 = plate.x0;

                    if (container_size.first * container_size.second - area == waste_lower_bound)
                    {
                        return area;
                    }
                }
            }

            if (best_area == -1)
            {
                return -1;
            }

            auto unpack_info = pack_one(plates, heap, container_size, best_item, best_plate_x0);
            if (IS_REAL_ITEM(best_item_index))
            {
                available_items[best_item_index].count--;
                current_solution.append({.item_width = best_item.width,
                                         .item_height = best_item.height,
                                         .x = unpack_info.packed_x,
                                         .y = plate.height});
            }

            int64_t smallest_width_index_backup = smallest_width_index;
            int64_t smallest_height_index_backup = smallest_height_index;

            if (best_item.count == 1 && smallest_width_index == best_item_index)
            {
                while (++smallest_width_index < available_items.size() && available_items[smallest_width_index].count == 0)
                {
                }

                smallest_width = smallest_width_index < available_items.size() ? available_items[smallest_width_index].width : container_size.first;
            }

            if (best_item.count == 1 && items_increasing_height[smallest_height_index].count == best_item_index) // .count hack
            {
                while (++smallest_height_index < items_increasing_height.size() && available_items[items_increasing_height[smallest_height_index].count].count == 0)
                {
                }

                smallest_height = smallest_height_index < items_increasing_height.size() ? items_increasing_height[smallest_height_index].height : container_size.second;
            }

            int64_t area = NestedPilot(plates,
                                       heap,
                                       container_size,
                                       available_items,
                                       current_area + best_item.width * best_item.height,
                                       current_best_area,
                                       current_waste + unpack_info.local_waste,
                                       best_area,
                                       nesting_level,
                                       current_solution,
                                       solution_out,
                                       waste_lower_bound,
                                       nullptr,
                                       items_increasing_height,
                                       smallest_width_index,
                                       smallest_height_index,
                                       is_top_level);

            unpack_one(plates, heap, container_size, best_item, unpack_info);

            if (IS_REAL_ITEM(best_item_index))
            {
                available_items[best_item_index].count++;
                current_solution.pop();
            }

            if (last_best && last_best->size() > 0)
            {
                (*last_best)[0].second = {area, 0, 0, 0, 0};

                std::sort(last_best->begin(), last_best->end(), [](auto a, auto b)
                          { return a.second > b.second; });
            }

            return area;
        }
    }

    /**
     * A simplified interface to NestedPilot (see the other function overload for commentary on parameters). This
     * overload is meant to be called by client code, while the other is considered private
     */
    int64_t NestedPilot(std::vector<Plate> &plates,
                        RandomHeap<int64_t> &heap,
                        std::pair<int64_t, int64_t> container_size,
                        std::vector<Item> &available_items,
                        int64_t current_area,
                        int64_t &current_best_area,
                        int64_t current_waste,
                        int64_t sibling_best,
                        int64_t nesting_level,
                        Solution &current_solution,
                        Solution &solution_out,
                        int64_t waste_lower_bound,
                        std::vector<std::pair<int64_t, std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t>>> *last_best = nullptr)
    {
        std::vector<Item> items_increasing_height = available_items;

        // HACK: use .count to keep track of original index after sorting
        for (int64_t i = 0; i < items_increasing_height.size(); i++)
        {
            items_increasing_height[i].count = i;
        }

        std::sort(items_increasing_height.begin(), items_increasing_height.end(), [](const Item &a, const Item &b)
                  { return a.height < b.height || (a.height == b.height && a.width < b.width); });

        int64_t smallest_width_index = 0;
        int64_t smallest_height_index = 0;

        return NestedPilot(plates,
                           heap,
                           container_size,
                           available_items,
                           current_area,
                           current_best_area,
                           current_waste,
                           sibling_best,
                           nesting_level,
                           current_solution,
                           solution_out,
                           waste_lower_bound,
                           last_best,
                           items_increasing_height,
                           smallest_width_index,
                           smallest_height_index,
                           true);
    }
}