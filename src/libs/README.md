Parsers for various formats of input file:
- `2dpacklib.h`: for 2DPackLib instance files
- `json.hpp`: for Óscar instance files
- `packfile.h`: for IDBS instance files
- `rakotonirainy.h`: for Rakotonirainy instance files

These data sources are described in the root README file