/**
 * Functions to parse files from the Rakotonirainy data source
 *
 * Format:
 * First line : width of initial strip
 * Other lines: Reference, Height, Width
 *
 */

#pragma once
#include <filesystem>
#include <fstream>
#include <vector>
#include <tuple>
#include <map>
#include <algorithm>
#include <math.h>

// returns vector<tuple<width, height, count>> where
// the first tuple is {container width, container height, -1}
// and the others are items
std::vector<std::tuple<int64_t, int64_t, int64_t>> read_rakotonirainy_file(std::string filepath)
{
    std::ifstream file(filepath);
    int64_t dummy;
    std::string comma;
    std::map<std::tuple<int64_t, int64_t>, int64_t> items;

    std::vector<std::tuple<int64_t, int64_t, int64_t>> result;

    int64_t container_width;
    int64_t container_height;
    int64_t item_count;
    int64_t total_item_area = 0;

    file >> container_width;
    std::getline(file, comma);

    while (true)
    {
        int64_t item_width;
        int64_t item_height;
        if (!(file >> dummy))
        {
            break;
        }
        std::getline(file, comma, ',');
        file >> item_height;
        std::getline(file, comma, ',');
        file >> item_width;

        total_item_area += item_width * item_height;

        if (items.count({item_width, item_height}) > 0)
        {
            items[{item_width, item_height}] = items[{item_width, item_height}] + 1;
        }
        else
        {
            items[{item_width, item_height}] = 1;
        }
    }

    container_height = (int64_t)ceil(total_item_area / (double)container_width);

    for (auto i : items)
    {
        result.push_back({std::get<0>(i.first), std::get<1>(i.first), i.second});
    }
    std::sort(result.begin(), result.end(), [](auto a, auto b)
              { return std::get<0>(a) < std::get<0>(b) ||
                       (std::get<0>(a) == std::get<0>(b) && std::get<1>(a) < std::get<1>(b)); });

    result.emplace(result.begin(), std::tuple<int64_t, int64_t, int64_t>({container_width, container_height, -1}));

    file.close();
    return result;
}