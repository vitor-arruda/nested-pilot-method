/**
 * Functions to parse files from the IDBS material
 *
 */

#pragma once
#include <regex>
#include <filesystem>
#include <fstream>

// returns vector<tuple<width, height, count>> where
// the first tuple is {container width, container height, -1}
// and the others are items
std::vector<std::tuple<int64_t, int64_t, int64_t>> read_packfile(std::string filepath)
{
    std::ifstream file(filepath);
    std::string dummy;
    std::map<std::tuple<int64_t, int64_t>, int64_t> items;

    std::vector<std::tuple<int64_t, int64_t, int64_t>> result;

    int64_t container_width;
    int64_t container_height;
    int64_t item_count;

    file >> dummy >> dummy;
    file >> dummy >> dummy;
    file >> container_width >> container_height;
    file >> item_count;

    while (item_count-- > 0)
    {
        int64_t item_width;
        int64_t item_height;
        file >> item_width >> item_height;

        if (items.count({item_width, item_height}) > 0)
        {
            items[{item_width, item_height}] = items[{item_width, item_height}] + 1;
        }
        else
        {
            items[{item_width, item_height}] = 1;
        }
    }

    for (auto i : items)
    {
        result.push_back({std::get<0>(i.first), std::get<1>(i.first), i.second});
        sort(result.begin(), result.end(), [](auto a, auto b)
             { return std::get<0>(a) < std::get<0>(b) ||
                      (std::get<0>(a) == std::get<0>(b) && std::get<1>(a) < std::get<1>(b)); });
    }

    result.emplace(result.begin(), std::tuple<int64_t, int64_t, int64_t>({container_width, container_height, -1}));

    file.close();
    return result;
}

// returns: <container area, solution waste, solution time, <width, height, x, y>[]>
std::tuple<int64_t, int64_t, double, std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>>> read_packfile_stats(std::string filepath)
{
    // std::cout << filepath << std::endl;
    std::ifstream file(filepath);

    std::string dummy;
    double solution_time;
    int64_t container_width;
    int64_t container_height;
    int64_t container_area;
    int64_t num_all_items;
    int64_t num_packed_items;
    int64_t item_width;
    int64_t item_height;
    int64_t packed_area = 0;
    std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>> packings;
    int64_t item_x;
    int64_t item_y;

    file >> dummy >> dummy;
    file >> dummy >> solution_time;
    file >> container_width >> container_height;
    file >> num_all_items;

    container_area = container_width * container_height;

    for (int64_t i = 0; i < num_all_items; i++)
    {
        file >> dummy >> dummy;
    }

    file >> num_packed_items;
    for (int64_t i = 0; i < num_packed_items; i++)
    {
        file >> item_width >> item_height >> item_x >> item_y >> dummy >> dummy >> dummy;
        packed_area += item_width * item_height;
        packings.push_back({item_width, item_height, item_x, item_y});
    }

    file.close();

    return {container_area, container_area - packed_area, solution_time, packings};
}

// returns: map<dataset name, tuple<container area, average waste, minimum waste, average time, all solutions>>
std::map<std::string, std::tuple<int64_t, double, int64_t, double, std::vector<std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>>>>> collect_packfiles_stats(std::string directory)
{
    std::map<std::string, std::vector<std::string>> prefixes;

    std::regex format("\\b([^ ]+)\\s+(\\d+).pack");
    std::smatch match;

    for (const auto &entry : std::filesystem::directory_iterator(directory))
    {
        std::string filename = entry.path().filename().string();
        std::regex_search(filename, match, format);
        std::string dataset = match[1];
        std::string number = match[2];
        filename = std::filesystem::path(directory) / filename;
        if (prefixes.count(dataset) == 0)
        {
            prefixes[dataset] = {filename};
        }
        else
        {
            prefixes[dataset].push_back(filename);
        }
    }

    std::map<std::string, std::tuple<int64_t, double, int64_t, double, std::vector<std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>>>>> result;

    for (auto x : prefixes)
    {
        auto dataset_name = x.first;
        auto filenames = x.second;

        int64_t minimum_waste = INT64_MAX;
        double average_waste = 0;
        double average_time = 0;
        int64_t count = 0;
        int64_t container_area = -1;
        std::vector<std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>>> packings;

        for (auto filename : filenames)
        {
            int64_t container_area_x;
            int64_t solution_waste;
            double solution_time;
            auto stats = read_packfile_stats(filename);
            container_area_x = std::get<0>(stats);
            solution_waste = std::get<1>(stats);
            solution_time = std::get<2>(stats);
            packings.push_back(std::get<3>(stats));

            if (container_area == -1)
            {
                container_area = container_area_x;
            }

            if (container_area != container_area_x)
            {
                std::cout << "container areas differed on dataset " << filename << ". Exiting..." << std::endl;
                exit(1);
            }

            if (solution_waste < minimum_waste)
            {
                minimum_waste = solution_waste;
            }

            average_waste += solution_waste;
            average_time += solution_time;
            count++;
        }

        average_waste /= (double)count;
        average_time /= (double)count;

        result[dataset_name] = {container_area, average_waste, minimum_waste, average_time, packings};
    }

    return result;
}