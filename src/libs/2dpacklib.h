/**
 * Functions to parse a file from 2DPackLib
 *
 * http://or.dei.unibo.it/library/2dpacklib
 *
 * The instances are in the following format:
 *
 * m
 * W H
 * i w_i h_i d_i b_i p_i (for each i=1,...,m)
 *
 * where:
 * m: number of items
 * W: width of the bin
 * H: height of the bin (-1 for 2D-SPP instances)
 * i: item ID
 * w_i: width of item i
 * h_i: height of item i
 * d_i: demand of item i (minimum number of copies to be packed)
 * b_i: maximum number of copies of item i
 * p_i: profit of item i
 *
 */

#pragma once
#include <filesystem>
#include <fstream>
#include <vector>
#include <tuple>
#include <map>
#include <algorithm>

// returns vector<tuple<width, height, count>> where
// the first tuple is {container width, container height, -1}
// and the others are items
std::vector<std::tuple<int, int, int>> read_file(std::string filepath)
{
    std::ifstream file(filepath);
    std::string dummy;
    std::map<std::tuple<int, int>, int> items;

    std::vector<std::tuple<int, int, int>> result;

    int container_width;
    int container_height;
    int item_count;

    file >> item_count;
    file >> container_width >> container_height;

    while (item_count-- > 0)
    {
        int item_width;
        int item_height;
        int item_count;
        file >> dummy >> item_width >> item_height >> dummy >> item_count >> dummy;

        if (items.count({item_width, item_height}) > 0)
        {
            items[{item_width, item_height}] = items[{item_width, item_height}] + item_count;
        }
        else
        {
            items[{item_width, item_height}] = item_count;
        }
    }

    for (auto i : items)
    {
        result.push_back({std::get<0>(i.first), std::get<1>(i.first), i.second});
    }
    std::sort(result.begin(), result.end(), [](auto a, auto b)
              { return std::get<0>(a) < std::get<0>(b) ||
                       (std::get<0>(a) == std::get<0>(b) && std::get<1>(a) < std::get<1>(b)); });

    result.emplace(result.begin(), std::tuple<int, int, int>({container_width, container_height, -1}));

    file.close();
    return result;
}