﻿// Copyright (c) 2012 Yan Krasny

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

/**
 * @brief A max-heap where each item has a tuple-priority and an id (number). Access to the entry with given id is O(1) in time
 *
 */

#pragma once

#include <algorithm>
#include <cmath>
#include <vector>
#include "./HeapEntry.h"

/*
Goal: to be able to refer to a specific entry from an outside data structure, in O(1)
That's why we store the index in HeapEntry
*/

template <class T>
class RandomHeap
{
private:
	std::vector<HeapEntry<T> *> heap;
	std::vector<HeapEntry<T> *> heap_backup;

public:
	RandomHeap() {}

	RandomHeap(std::vector<HeapEntry<T> *> &origVec)
	{
		for (size_t i = 0; i < origVec.size(); i++)
		{
			this->insert(origVec[i]);
		}
	}

	bool empty() const
	{
		return heap.size() <= 0;
		// return heap.empty();
	}

	void clear()
	{
		heap.clear();
	}

	HeapEntry<T> &getAtIndex(int64_t pos)
	{
		if (pos >= 0 && pos < heap.size())
		{
			return *heap[pos];
		}
	}

	HeapEntry<T> *getMax()
	{
		return heap[0];
	}

	HeapEntry<T> extractMax()
	{
		return *extractRandom(0);
	}

	int64_t insert(HeapEntry<T> *item)
	{
		heap.push_back(item);
		int64_t index = heapifyUp(heap.size() - 1);
		heap[index]->setIndex(index);
		return index;
	}

	void changePriority(HeapEntry<T> *item, std::tuple<int64_t, int64_t, int64_t> new_priority)
	{
		auto old_priority = item->getPriority();
		item->setPriority(new_priority);
		if (new_priority > old_priority)
		{
			heapifyUp(item->getIndex());
		}
		else if (new_priority < old_priority)
		{
			heapifyDown(item->getIndex());
		}
	}

	int64_t heapifyUp(int64_t pos)
	{
		auto my_priority = heap[pos]->getPriority();
		while (true)
		{
			// if at any point during this alg we go out of bounds, we're done
			if (pos < 1 || pos >= heap.size())
			{
				return pos;
			}

			int64_t parent = (pos - 1) / 2;
			auto parent_priority = heap[parent]->getPriority();

			// the current element is greater than its parent, swap it up and continue with the parent's position
			if (my_priority > parent_priority)
			{
				// Keeping the indexes correct while swapping
				heap[pos]->setIndex(parent);
				heap[parent]->setIndex(pos);

				// Swap
				std::swap(heap[pos], heap[parent]);

				// Next position we're looking at is the parent
				pos = parent;
				continue;
			}

			break;
		}
		return pos;
	}

	void heapifyDown(int64_t pos)
	{
		auto my_priority = heap[pos]->getPriority();

		while (true)
		{
			int64_t leftChildIndex = 2 * pos + 1;
			int64_t rightChildIndex = 2 * pos + 2;

			if (leftChildIndex >= heap.size())
			{
				// there is no left child, so we're at a leaf, done
				break;
			}

			auto leftchild_priority = heap[leftChildIndex]->getPriority();
			std::tuple<int64_t, int64_t, int64_t> rightchild_priority;

			if (rightChildIndex < heap.size())
			{
				rightchild_priority = heap[rightChildIndex]->getPriority();
				int64_t largest_child = leftchild_priority > rightchild_priority ? leftChildIndex : rightChildIndex;
				auto largest_child_priority = leftchild_priority > rightchild_priority ? leftchild_priority : rightchild_priority;

				if (my_priority < largest_child_priority)
				{
					// keep indexes updated
					heap[pos]->setIndex(largest_child);
					heap[largest_child]->setIndex(pos);

					// swap
					std::swap(heap[pos], heap[largest_child]);

					pos = largest_child;

					continue;
				}
			}
			else if (my_priority < leftchild_priority)
			{
				// keep indexes updated
				heap[pos]->setIndex(leftChildIndex);
				heap[leftChildIndex]->setIndex(pos);

				// swap
				std::swap(heap[pos], heap[leftChildIndex]);

				// the current element is smaller than its left child, swap and check that position
				pos = leftChildIndex;

				continue;
			}

			break;
		}
	}

	/**
	 * @brief Delete an element given its key `pos`, in O(1) time
	 */
	void deleteRandom(int64_t pos)
	{
		// This messes up the index field
		heap[pos] = heap.back();

		// So reset it
		heap[pos]->setIndex(pos);

		// Remove the last element
		heap.pop_back();

		if (heap.size() > 0)
		{
			heapifyUp(pos);
			heapifyDown(pos);
		}
	}

	HeapEntry<T> *extractRandom(int64_t pos)
	{
		HeapEntry<T> *item = heap[pos];
		deleteRandom(pos);
		return item;
	}

	void backup()
	{
		heap_backup = heap;
	}

	void restore()
	{
		heap = heap_backup;
		for (int64_t i = 0; i < heap.size(); i++)
		{
			heap[i]->setIndex(i);
		}
	}

	std::vector<HeapEntry<T> *> &raw_heap()
	{
		return heap;
	}

	~RandomHeap()
	{
		heap.clear();
	}
};
