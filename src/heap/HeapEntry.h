// Copyright (c) 2012 Yan Krasny

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#pragma once

#include <algorithm>
#include <vector>

template <class T>
class HeapEntry
{
private:
	int64_t index; // the object must know where it is, so that some other reference can find it in O(1) inside the heap
	std::tuple<int64_t, int64_t, int64_t> priority;
	T key;

public:
	HeapEntry(std::tuple<int64_t, int64_t, int64_t> priority, T key) : priority(priority), index(-1), key(key) {}
	HeapEntry() : priority({0, 0, 0}), index(-1) {}

	std::tuple<int64_t, int64_t, int64_t> getPriority() const;
	void setPriority(int64_t priority1, int64_t priority2 = 0, int64_t priority3 = 0);
	void setPriority(std::tuple<int64_t, int64_t, int64_t> priority);

	~HeapEntry();

	int64_t getIndex() const;

	void setIndex(int64_t index);

	void setKey(T newkey)

	{
		key = newkey;
	}

	T getKey() const;
};

template <class T>
std::tuple<int64_t, int64_t, int64_t> HeapEntry<T>::getPriority() const
{
	return priority;
}

template <class T>
void HeapEntry<T>::setPriority(int64_t priority1, int64_t priority2, int64_t priority3)
{
	this->priority = {priority1, priority2, priority3};
}

template <class T>
void HeapEntry<T>::setPriority(std::tuple<int64_t, int64_t, int64_t> priority)
{
	this->priority = priority;
}

template <class T>
int64_t HeapEntry<T>::getIndex() const
{
	return index;
}

template <class T>
void HeapEntry<T>::setIndex(int64_t index)
{
	this->index = index;
}

template <class T>
HeapEntry<T>::~HeapEntry()
{
	priority = {0, 0, 0};
	index = -1;
}

template <class T>
T HeapEntry<T>::getKey() const
{
	return key;
}
