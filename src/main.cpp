#include <cmath>
#include <unistd.h>
#include <signal.h>
#include <chrono>
#include "./libs/json.hpp"
#include "./libs/hasending.h"
#include "./libs/packfile.h"
#include "./libs/2dpacklib.h"
#include "./libs/rakotonirainy.h"
#include "./nested-pilot.h"

using namespace Valleys;

double total_item_pack_count_all_levels = 0;
Solution solution_out_np(std::numeric_limits<int64_t>::max());
clock_t np_start_checkpoint = std::clock();
clock_t np_last_checkpoint = std::clock();

void sigalrm_handler(int32_t sig)
{
    int64_t area = 0;
    for (auto x : solution_out_np.packings)
    {
        area += x.item_width * x.item_height;
    }
    np_last_checkpoint = std::clock();
    std::cout << "in " << (((double)np_last_checkpoint - np_start_checkpoint) / (double)CLOCKS_PER_SEC) << " seconds" << std::endl;
    std::cout << "terminated when expanding child number  "
              << NestedPilot::g_child_expansion_number
              << "/"
              << NestedPilot::g_child_total_number
              << " at depth "
              << NestedPilot::g_depth << std::endl;
    std::cout << "best solution until now: " << area << std::endl;
    std::cout << "waste: " << solution_out_np.waste_area << std::endl;
    solution_out_np.print();
    std::cout << "total nodes visited: " << total_item_pack_count_all_levels + Valleys::total_item_pack_count << std::endl;
    std::cout << "exiting" << std::endl;
    exit(0);
}

/**
 * @details
 * Possible command line arguments:
 *  - -input-file <input file path>
 *      Specifies the input file describing container and items to pack. Ignored when -mode is "IDBS-stats".
 *      - If the file has extension .json, it is inferred to be from the Óscar data source, and is parsed with libs/json.hpp
 *      - If the file path is inside the Rakotonirainy/ directory, it is inferred to be from the Rakotonirainy data source, and
 *        is parsed with libs/rakotonirainy.h
 *      - If the file has extension .ins2D, it is inferred to be from the 2dPackLib data source, and is parsed with
 *        libs/2dpacklib.h
 *      - If the file has extension .pack, it is inferred to be from the IDBS data source, and is parsed with
 *        libs/packfile.h
 *  - -mode <mode>
 *      Mode can be one of "dry-run", "NestedPilot" or "IDBS-stats" (without quotes).
 *      "dry-run" prints input-file information and exits.
 *      "NestedPilot" runs IterativeNesting (Nested Pilot method with iteratively increasing
 *      nesting level) against the input-file.
 *      "IDBS-stats" reads all IDBS solution files and prints their information.
 *  - -rotate-items
 *      If specified, rotates all items from input-file by 90 degrees
 *  - -sleep <seconds>
 *      Specifies a time to wait between printing input-file information and starting NestedPilot. Default 5 seconds
 *  - -time-limit <seconds>
 *      Time limit to run NestedPilot. The program exits abruptly using a signal handler (timer) when the time is out
 */
int main(int argc, char *argv[])
{
#ifdef MISSING_METRIC
    std::cout
        << "You must compile with one of -DUSE_HEURISTIC_H0, -DUSE_HEURISTIC_H1 or -DUSE_HEURISTIC_H2"
        << std::endl;
#endif

    std::pair<int64_t, int64_t> container_size = {100, 100};

    std::vector<Item> available_items(container_size.first - 1, {.count = 1});

    bool rotate_items = false;
    for (size_t i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "-rotate-items")
        {
            rotate_items = true;
        }
    }

    std::string input_file = "<none>";
    for (size_t i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "-input-file")
        {
            input_file = argv[i + 1];
            if (hasEnding(argv[i + 1], ".json"))
            {
                std::ifstream input(argv[i + 1]);
                nlohmann::json j;
                input >> j;

                std::string widthProp = "Length";
                std::string heightProp = "Height";

                if (rotate_items)
                {
                    std::swap(widthProp, heightProp);
                }

                int64_t total_item_area = 0;
                for (int64_t index = 0; index < j["Items"].size(); index++)
                {
                    auto i = j["Items"][index];
                    total_item_area += (int64_t)i[widthProp] * (int64_t)i[heightProp] * (int64_t)i["Demand"];
                }

                int64_t c_width = j["Objects"][0]["Length"];
                int64_t c_height = j["Objects"][0]["Height"].is_null()
                                       ? ((int64_t)ceil(total_item_area / (double)c_width))
                                       : (int64_t)j["Objects"][0]["Height"];

                container_size = {c_width, c_height};
                available_items.resize(0);

                int64_t size = j["Items"].size();

                std::sort(j["Items"].begin(), j["Items"].begin() + size, [&widthProp, &heightProp](auto a, auto b)
                          { return a[widthProp] < b[widthProp] || (a[widthProp] == b[widthProp] && a[heightProp] < b[heightProp]); });
                for (int64_t index = 0; index < size; index++)
                {
                    auto i = j["Items"][index];
                    available_items.push_back({.width = i[widthProp],
                                               .height = i[heightProp],
                                               .count = i["Demand"]});
                }
            }
            else if (hasEnding(argv[i + 1], ".pack"))
            {
                auto data = read_packfile(argv[i + 1]);

                int64_t item_total_area = 0;
                for (int64_t i = 1; i < data.size(); i++)
                {
                    item_total_area += std::get<0>(data[i]) * std::get<1>(data[i]) * std::get<2>(data[i]);
                }

                int64_t container_width = std::get<0>(data[0]);
                int64_t container_height = std::get<1>(data[0]);
                if (container_height == -1)
                {
                    container_height = ((int64_t)ceil(item_total_area / (double)container_width));
                }

                container_size = {container_width, container_height};
                available_items.resize(0);

                int64_t size = data.size();

                for (int64_t i = 1; i < size; i++)
                {
                    available_items.push_back({.width = !rotate_items ? std::get<0>(data[i]) : std::get<1>(data[i]),
                                               .height = !rotate_items ? std::get<1>(data[i]) : std::get<0>(data[i]),
                                               .count = std::get<2>(data[i])});
                    if (available_items[available_items.size() - 1].width <= 0 || available_items[available_items.size() - 1].height <= 0 || available_items[available_items.size() - 1].count <= 0)
                    {
                        std::cout << "BUG" << std::endl;
                        exit(1);
                    }
                }
                std::sort(available_items.begin(), available_items.end(), [](const Item &a, const Item &b)
                          { return a.width < b.width || (a.width == b.width && a.height < b.height); });
            }
            else if (hasEnding(argv[i + 1], ".ins2D"))
            {
                auto data = read_file(argv[i + 1]);

                int64_t item_total_area = 0;
                for (int64_t i = 1; i < data.size(); i++)
                {
                    item_total_area += std::get<0>(data[i]) * std::get<1>(data[i]) * std::get<2>(data[i]);
                }

                int64_t container_width = std::get<0>(data[0]);
                int64_t container_height = std::get<1>(data[0]);
                if (container_height == -1)
                {
                    container_height = ((int64_t)ceil(item_total_area / (double)container_width));
                }

                container_size = {container_width, container_height};

                available_items.resize(0);

                int64_t size = data.size();

                for (int64_t i = 1; i < size; i++)
                {
                    available_items.push_back({.width = !rotate_items ? std::get<0>(data[i]) : std::get<1>(data[i]),
                                               .height = !rotate_items ? std::get<1>(data[i]) : std::get<0>(data[i]),
                                               .count = std::get<2>(data[i])});
                    if (available_items[available_items.size() - 1].width <= 0 || available_items[available_items.size() - 1].height <= 0 || available_items[available_items.size() - 1].count <= 0)
                    {
                        std::cout << "BUG" << std::endl;
                        exit(1);
                    }
                }
                std::sort(available_items.begin(), available_items.end(), [](const Item &a, const Item &b)
                          { return a.width < b.width || (a.width == b.width && a.height < b.height); });
            }
            else if (input_file.find(std::string("Rakotonirainy")) != std::string::npos)
            {
                auto data = read_rakotonirainy_file(argv[i + 1]);

                int64_t container_width = std::get<0>(data[0]);
                int64_t container_height = std::get<1>(data[0]);

                container_size = {container_width, container_height};

                available_items.resize(0);

                int64_t size = data.size();

                for (int64_t i = 1; i < size; i++)
                {
                    available_items.push_back({.width = !rotate_items ? std::get<0>(data[i]) : std::get<1>(data[i]),
                                               .height = !rotate_items ? std::get<1>(data[i]) : std::get<0>(data[i]),
                                               .count = std::get<2>(data[i])});
                    if (available_items[available_items.size() - 1].width <= 0 || available_items[available_items.size() - 1].height <= 0 || available_items[available_items.size() - 1].count <= 0)
                    {
                        std::cout << "BUG" << std::endl;
                        exit(1);
                    }
                }
                std::sort(available_items.begin(), available_items.end(), [](const Item &a, const Item &b)
                          { return a.width < b.width || (a.width == b.width && a.height < b.height); });
            }
            else
            {
                std::cout << "Unknown input file format. Exiting..." << std::endl;
                exit(1);
            }
        }
    }

    int64_t total_area = 0;
    for (auto item : available_items)
    {
        total_area += item.width * item.height * item.count;
    }

    if (total_area > container_size.first * container_size.second)
    {
        std::cout << "total area of items is greater than the container area" << std::endl;
        exit(1);
    }

    int64_t waste_lower_bound = container_size.first * container_size.second - total_area;

    std::string mode = "<unknown>";
    for (size_t i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "-mode")
        {
            mode = argv[i + 1];
        }
    }

    int64_t sleep_time = 5;
    for (size_t i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "-sleep")
        {
            sleep_time = atoi(argv[i + 1]);
        }
    }

    int64_t time_limit = -1; // seconds
    for (size_t i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "-time-limit")
        {
            time_limit = atoi(argv[i + 1]);
        }
    }

    int64_t small_items_count = 0;
    // total number of items
    int64_t total_items_count = 0;
    for (auto x : available_items)
    {
        if (2 * x.height < container_size.second && 2 * x.width < container_size.first)
        {
            small_items_count += x.count;
        }
        total_items_count += x.count;
    }

    int64_t nesting_level = 1;

    if (mode != "IDBS-stats")
    {
        std::cout << "input_file " << input_file << std::endl;
        std::cout << "container size "
                  << "(" << container_size.first << "," << container_size.second << ")" << std::endl;
        std::cout << "available_items ";
        for (auto item : available_items)
        {
            std::cout << "(" << item.width << "," << item.height << ")";
            if (item.count > 1)
            {
                std::cout << "[" << item.count << "]";
            }
            std::cout << " ";
        }
        std::cout << std::endl;
        std::cout << "waste_lower_bound " << waste_lower_bound << std::endl;
        std::cout << "mode " << mode << std::endl;
#ifdef USE_HEURISTIC_H0
        std::cout << "heuristic H0" << std::endl;
#endif
#ifdef USE_HEURISTIC_H1
        std::cout << "heuristic H1" << std::endl;
#endif
#ifdef USE_HEURISTIC_H2
        std::cout << "heuristic H2" << std::endl;
#endif
        std::cout << "nesting_level " << nesting_level << std::endl;
        std::cout << "sleep_time " << sleep_time << std::endl;
        std::cout << "time_limit " << time_limit << std::endl;
        std::cout << std::endl;

        std::cout << "total items: " << total_items_count << std::endl;
        std::cout << "fraction of small boxes: " << ((double)small_items_count) / total_items_count << std::endl;
        std::cout << "sum of item areas / container area: " << (double)total_area / (double)(container_size.first * container_size.second) << std::endl;
    }

    sleep(sleep_time);

    if (mode == "dry-run")
    {
        if (input_file == "<none>")
        {
            std::cout << "specify an input file with -input-file" << std::endl;
            exit(1);
        }
        return 0;
    }
    else if (mode == "NestedPilot")
    {
        if (input_file == "<none>")
        {
            std::cout << "specify an input file with -input-file" << std::endl;
            exit(1);
        }
        uint seed = std::chrono::system_clock::now().time_since_epoch().count();
        srand(seed);
        std::vector<Plate> plates;
        RandomHeap<int64_t> heap;
        create_initial_configuration(container_size, &heap, &plates);

        Solution current_solution(container_size.first * container_size.second);
        int64_t best_area = 0;

        if (time_limit > 0)
        {
            signal(SIGALRM, &sigalrm_handler);
            alarm(time_limit);
        }

        std::vector<std::pair<int64_t, std::tuple<int64_t, int64_t, int64_t, int64_t, int64_t>>> last_best;

        np_start_checkpoint = std::clock();
        NestedPilot::g_start_checkpoint = std::clock();

        total_item_pack_count_all_levels = 0;
        while (nesting_level < total_items_count)
        {
            std::cout << "setting nesting_level to " << nesting_level << std::endl;

            Valleys::total_item_pack_count = 0;
            int64_t area = NestedPilot::NestedPilot(
                plates,
                heap,
                container_size,
                available_items,
                0 /** current_area */,
                best_area,
                0 /** current_waste */,
                0 /** sibling_best */,
                nesting_level,
                current_solution,
                solution_out_np,
                waste_lower_bound,
                &last_best);

            if (container_size.first * container_size.second - area == waste_lower_bound)
            {
                std::cout << "finished ahead of time" << std::endl;
                sigalrm_handler(-1);
            }

            std::cout << "nesting_level=" << nesting_level << " tree had " << Valleys::total_item_pack_count << " nodes" << std::endl;
            nesting_level++;

            total_item_pack_count_all_levels += Valleys::total_item_pack_count;
        }

        std::cout << "waste: " << container_size.first * container_size.second - best_area << std::endl;
        std::cout << "total visited nodes: " << total_item_pack_count_all_levels << std::endl;
    }
    else if (mode == "IDBS-stats")
    {
        std::map<std::string, std::tuple<int64_t, double, int64_t, double, std::vector<std::vector<std::tuple<int64_t, int64_t, int64_t, int64_t>>>>> result = collect_packfiles_stats("results/IDBS-results");

        std::cout << "dataset_name"
                  << "\t\t"
                  << "container_area"
                  << "\t\t"
                  << "average_waste"
                  << "\t\t"
                  << "minimum_waste"
                  << "\t\t"
                  << "average_time"
                  << "\t\t" << std::endl;

        for (auto x : result)
        {
            auto dataset_name = x.first;
            int64_t container_area;
            double average_waste;
            int64_t minimum_waste;
            double average_time;
            auto stats = x.second;
            container_area = std::get<0>(stats);
            average_waste = std::get<1>(stats);
            minimum_waste = std::get<2>(stats);
            average_time = std::get<3>(stats);

            std::cout << dataset_name << "\t\t\t"
                      << container_area << "\t\t\t"
                      << average_waste << "\t\t\t"
                      << minimum_waste << "\t\t\t"
                      << average_time << "\t\t\t"
                      << std::endl;
        }
    }
    else
    {
        std::cout << "unknown mode \"" << mode << "\". Exiting..." << std::endl;
    }
}