/**
 * Structs to represent a skyline, items (real and false items), plates (segments of the skyline), valleys
 * (a particular type of plate).
 *
 * Also functions `pack_one` and `unpack_one` to pack/unpack an item in the container, automatically updating the
 * skyline.
 */

#pragma once
#include <list>
#include <iostream>
#include <ctime>
#include "./heap/RandomHeap.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define ABS(x) ((x) > 0 ? (x) : -(x))

/**
 * Real items have non-negative index on the vector of available items.
 * False items (for valley flattening) have -1 index
 *
 */
#define IS_REAL_ITEM(item_index) ((item_index) >= 0)

/**
 * Default geometry of the false item: width=-1 and area=0.
 * This geometry may be altered for particular purposes, in which case
 * the item will be identified as a false item because of its index on the
 * list of available items (FALSE_ITEM_INDEX below)
 */
#define FALSE_ITEM_GEOMETRY_DEFAULT \
	{                               \
		.width = -1, .height = 0    \
	}

#define FALSE_ITEM_WIDTH_DEFAULT -1

#define FALSE_ITEM_INDEX -1

namespace Valleys
{

	/**
	 * @brief Represents a horizontal segment of the skyline
	 *
	 * @details
	 *
	 * On the example diagram below, there are 4 plates in the container (four horizontal segments on the skyline).
	 * Only plate 2 is a valley. On this plate:
	 * - width=4 (the plate is 4 units wide)
	 * - height=2 (the plate is on y=2)
	 * - x0=5 (the plate starts on x=5)
	 * - prev_x0=0 (x0 of plate 1)
	 * - left_wall=4 (height of left wall)
	 * - right_wall=2 (height of right wall)
	 * - valley_height=2 (mininum of left and right walls)
	 * - taller_wall=4
	 * - top_gap=7 (container height - height = 9 - 2 = 7)
	 *
	 * width, height, x0 and prev_x0 are calculated for all plates. The other properties are only calculated
	 * for valleys (when the plate enters the valley heap on `enter_heap()`)
	 *
	 * y axis
	 *   8 ▲  ┌───────────┐
	 *   7 │  │           │
	 *   6 │  │         ┌─┤
	 *   5 │  ├────┐    │4│
	 *   4 │  │ 1  │    │ │
	 *   3 │  │    │  ┌─┘ │
	 *   2 │  │    └──┘3  │
	 *   1 │  │     2     │
	 *   0 │  └───────────┘
	 *
	 *        ────────────► x axis
	 *        0123456789111
	 *                  012
	 *
	 */
	struct Plate
	{
		// basic information calculated for all plates
		int64_t width;
		int64_t height;
		int64_t x0; // x position where the plate starts
		int64_t prev_x0;
		std::pair<int64_t, int64_t> container_size;

		// extra information calculated only for valleys
		int64_t valley_height;
		int64_t left_wall;
		int64_t right_wall;
		int64_t taller_wall;
		int64_t top_gap;

		std::vector<Plate> *plates; // skyline structure
		/**
		 * @brief heap is a max-heap of valleys. When a plate (valley) enters the heap, it assigns itself
		 * a priority (for example, -width). The heap can find the highest-priority valley in O(1) time
		 */
		RandomHeap<int64_t> *heap;
		HeapEntry<int64_t> heap_entry; // representation of this plate in the heap

		Plate() {}

		Plate(std::vector<Plate> *plates,
			  RandomHeap<int64_t> *heap,
			  int64_t width,
			  int64_t height,
			  int64_t x0,
			  int64_t prev_x0,
			  std::pair<int64_t, int64_t> container_size) : plates(plates),
															heap(heap),
															width(width),
															height(height),
															x0(x0),
															heap_entry({-width, 0, 0}, x0),
															prev_x0(prev_x0),
															container_size(container_size) {}

		Plate &operator=(const Plate &other)
		{
			width = other.width;
			height = other.height;
			x0 = other.x0;
			prev_x0 = other.prev_x0;
			heap_entry = other.heap_entry;
			valley_height = other.valley_height;
			left_wall = other.left_wall;
			right_wall = other.right_wall;
			container_size = other.container_size;

			return *this;
		}

		/**
		 * @brief Inserts this plate on the valley heap.
		 * Precondition: this plate is a valley.
		 *
		 */
		inline void enter_heap()
		{
			left_wall = MIN(prev().height, container_size.second) - height;
			right_wall = MIN(next().height, container_size.second) - height;

			valley_height = MIN(left_wall, right_wall);
			taller_wall = MAX(left_wall, right_wall);

			top_gap = container_size.second - height;

			// prefer the minimum-width valley
			heap_entry.setPriority(-width, -height, -x0);

			heap->insert(&heap_entry);
		}

		inline void exit_heap()
		{
			heap->deleteRandom(heap_entry.getIndex());
		}

		/**
		 * @brief Gets the next segment to the right
		 *
		 * @return Plate&
		 */
		inline Plate &next()
		{
			return (*plates)[x0 + width + 1];
		}

		/**
		 * @brief Gets the previous segment to the left
		 *
		 * @return Plate&
		 */
		inline Plate &prev()
		{
			return (*plates)[prev_x0 + 1];
		}

		inline bool is_valley()
		{
			return x0 + 1 > 0 && x0 + 1 < plates->size() - 1 && prev().height > height && next().height > height;
		}

		/**
		 * @brief When the container is full and no space remains, there is still a pseudo-valley. Returns
		 * true if this is the case
		 *
		 */
		inline bool is_final_valley()
		{
			return valley_height == 0;
		}
	};

	/**
	 * @brief Shortcut to get the priority valley on the heap
	 */
	inline Plate &min_valley(std::vector<Plate> &plates, RandomHeap<int64_t> &heap)
	{
		return plates[heap.getMax()->getKey() + 1];
	}

	/**
	 * @brief An item with geometry and multiplicity (how many copies we have)
	 */
	struct Item
	{
		int64_t width;
		int64_t height;
		int64_t count;
	};

	/**
	 * @brief Represents an item packed somewhere on the container
	 */
	struct Packing
	{
		int64_t item_width;
		int64_t item_height;
		int64_t x;
		int64_t y;
	};

	/**
	 * @brief Represents a sequence of items packed on the container. Can append and pop items from the sequence,
	 * and manages the waste area automatically
	 */
	struct Solution
	{
		std::vector<Packing> packings;
		int64_t waste_area;
		int64_t packing_count = 0; // counter for all packings found during search (could be a global variable...)
		bool clean = false;

		Solution(int64_t waste_area) : waste_area(waste_area) {}

		Solution() = delete;

		void append(Packing p)
		{
			packings.push_back(p);
			waste_area -= p.item_width * p.item_height;
			clean = true;
		}

		void pop()
		{
			waste_area += packings.back().item_width * packings.back().item_height;
			packings.pop_back();
			if (clean)
			{
				packing_count++;
			}
			clean = false;
		}

		void print(bool linefeed = true)
		{
			for (auto x : packings)
			{
				std::cout << "(" << x.item_width << "," << x.item_height << ")(" << x.x << "," << x.y << ") ";
			}
			if (linefeed)
			{
				std::cout << std::endl;
			}
		}
	};

	/**
	 * @brief Information about where an item is packed on the container. Necessary if we want to
	 * remove this item in the future (with function `unpack_one`)
	 *
	 */
	struct UnpackInformation
	{
		Plate *prev;
		Plate *next;
		Plate *plate;
		Plate *new_plate;
		Plate new_plate_backup;
		Plate plate_backup;
		Plate *old_next;
		int64_t extension;
		int64_t prev_diff;
		int64_t next_diff;
		int64_t height_diff;
		int64_t packing_case;
		int64_t local_waste; // 0 if not a planification (flattening)
		int64_t packed_x;
		int64_t packed_y;

		UnpackInformation() {}
	};

	/**
	 * @brief Can be reset by client code at any time.
	 * Counts the number of items packed in the
	 * course of the algorithm
	 *
	 */
	double total_item_pack_count = 0;

	// private
	clock_t start_checkpoint = std::clock();
	clock_t last_checkpoint = std::clock();
	clock_t checkpoint = std::clock();

	/**
	 * Packs an item in a plate (plate_x0), and updates the plates and heap.
	 * item.width==-1 indicates to flatten the valley.
	 * precondition: `item` truly fits inside the specified plate.
	 *
	 * @return UnpackInformation. Struct needed in `unpack_one` if we want to remove the item in the future
	 */
	UnpackInformation pack_one(
		std::vector<Plate> &plates,
		RandomHeap<int64_t> &heap,
		std::pair<int64_t, int64_t> container_size,
		Item item,
		int64_t plate_x0)
	{
		total_item_pack_count++;
		UnpackInformation info;
		Plate &plate = plates[plate_x0 + 1];
		Plate &prev = plate.prev();
		Plate &next = plate.next();

		int64_t extension = MIN(prev.height, container_size.second) - plate.height;
		int64_t right_extension = MIN(next.height, container_size.second) - plate.height;

		info.extension = extension;
		info.plate = &plate;
		info.prev = &prev;
		info.next = &next;
		info.local_waste = 0;

		info.packed_x = plate.x0;
		info.packed_y = plate.height;

		if (item.width == FALSE_ITEM_WIDTH_DEFAULT)
		{
			// Fill with false item until the minimum bordering height
			int64_t fill_height = MIN(container_size.second, MIN(prev.height, next.height));
			int64_t height_diff = fill_height - plate.height;
			int64_t wasted_area = plate.width * height_diff;

			plate.exit_heap();
			plate.height += height_diff;
			int64_t prev_diff = plate.height - prev.height;
			int64_t next_diff = plate.height - next.height;

			if (prev_diff == 0 && next_diff == 0)
			{
				prev.width += plate.width + next.width;
				prev.next().prev_x0 = prev.x0;
				if (prev.is_valley())
				{
					prev.enter_heap();
				}
			}
			else if (prev_diff == 0 && next_diff < 0)
			{
				prev.width += plate.width;
				prev.next().prev_x0 = prev.x0;
				if (prev.is_valley())
				{
					prev.enter_heap();
				}
			}
			else if (next_diff == 0 && prev_diff < 0)
			{
				plate.width += next.width;
				plate.next().prev_x0 = plate.x0;

				if (plate.is_valley())
				{
					plate.enter_heap();
				}
			}
			else // both are negative (subrectangle)
			{
				plate.enter_heap();
			}

			info.prev_diff = prev_diff;
			info.next_diff = next_diff;
			info.height_diff = height_diff;
			info.packing_case = 4;
			info.local_waste = wasted_area;

			return info;
		}

		else if (item.width == plate.width)
		{
			plate.exit_heap();
			plate.height += item.height;
			int64_t prev_diff = plate.height - prev.height;
			int64_t next_diff = plate.height - next.height;

			if (prev_diff < 0 && next_diff < 0)
			{
				plate.enter_heap();
			}
			else if (prev_diff > 0 && next_diff > 0)
			{
				if (prev.is_valley())
				{
					prev.enter_heap();
				}

				if (next.is_valley())
				{
					next.enter_heap();
				}
			}
			else if (prev_diff > 0) // therefore next_diff <= 0
			{
				if (prev.is_valley())
				{
					prev.enter_heap();
				}

				if (next_diff == 0)
				{
					// merge next
					plate.width += next.width;
					plate.next().prev_x0 = plate.x0;
				}
			}
			else if (next_diff > 0) // therefore prev_diff <= 0
			{
				if (next.is_valley())
				{
					next.enter_heap();
				}

				if (prev_diff == 0)
				{
					// merge prev
					prev.width += plate.width;
					next.prev_x0 = prev.x0;
				}
			}
			else if (prev_diff < 0) // and next_diff == 0
			{
				// merge next
				plate.width += next.width;
				plate.next().prev_x0 = plate.x0;

				if (plate.is_valley())
				{
					plate.enter_heap();
				}
			}
			else if (next_diff < 0) // and prev_diff == 0
			{
				// merge prev
				prev.width += plate.width;
				next.prev_x0 = prev.x0;

				if (prev.is_valley())
				{
					prev.enter_heap();
				}
			}
			else // both are 0
			{
				// merge both ways
				prev.width += plate.width + next.width;
				prev.next().prev_x0 = prev.x0;

				if (prev.is_valley())
				{
					prev.enter_heap();
				}
			}

			info.packing_case = 0;
			info.prev_diff = prev_diff;
			info.next_diff = next_diff;

			return info;
		}

#ifdef USE_HEURISTIC_H0
		else if (item.height == extension && plate.left_wall >= plate.right_wall)
#else
		else if (item.height == extension)
#endif
		{
			// there are 2 kinds of extensions:
			// (i) extend a previous plate that is not the first plate
			// (ii) (pseudo-)extend the first plate (happens when the algorithm packs a subrectangle)
			if (prev.height <= container_size.second)
			{
				plate.exit_heap();
				prev.width += item.width;
				auto &new_plate = prev.next();
				auto new_plate_backup = new_plate;
				new_plate.width = plate.width - item.width;
				new_plate.prev_x0 = prev.x0;
				new_plate.height = plate.height;
				next.prev_x0 = new_plate.x0;
				new_plate.enter_heap();

				info.new_plate = &new_plate;
				info.new_plate_backup = new_plate_backup;
				info.packing_case = 1;

				return info;
			}
			else
			{
				plate.exit_heap();
				auto &new_plate = plates[plate.x0 + item.width + 1];
				auto new_plate_backup = new_plate;
				new_plate.prev_x0 = plate.x0;
				new_plate.width = plate.width - item.width;
				new_plate.height = plate.height;
				new_plate.next().prev_x0 = new_plate.x0;
				auto plate_backup = plate;
				plate.height += item.height;
				plate.width = item.width;
				new_plate.enter_heap();

				info.new_plate = &new_plate;
				info.new_plate_backup = new_plate_backup;
				info.plate_backup = plate_backup;
				info.packing_case = 2;

				return info;
			}
		}

#ifdef USE_HEURISTIC_H0
		else if (item.height == right_extension && plate.right_wall > plate.left_wall)
#else
		else if (item.height == right_extension)
#endif
		{
			info.packed_x = plate.x0 + plate.width - item.width;

			// there are 2 kinds of extensions:
			// (i) extend a next plate that is not the last plate
			// (ii) (pseudo-)extend the last plate (happens when the algorithm packs a subrectangle)
			if (next.height <= container_size.second)
			{
				plate.exit_heap();
				auto &old_next = plate.next();
				plate.width -= item.width;
				auto &new_plate = plates[plate.x0 + plate.width + 1];
				auto new_plate_backup = new_plate;
				new_plate.height = plate.height + item.height;
				new_plate.prev_x0 = plate.x0;
				new_plate.width = item.width + old_next.width;
				old_next.next().prev_x0 = new_plate.x0;
				plate.enter_heap();

				info.new_plate = &new_plate;
				info.new_plate_backup = new_plate_backup;
				info.old_next = &old_next;
				info.packing_case = 5;
			}
			else
			{
				plate.exit_heap();
				plate.width -= item.width;
				auto &new_plate = plates[plate.x0 + plate.width + 1];
				auto new_plate_backup = new_plate;
				new_plate.prev_x0 = plate.x0;
				new_plate.width = item.width;
				new_plate.height = plate.height + item.height;
				new_plate.next().prev_x0 = new_plate.x0;
				plate.enter_heap();

				info.new_plate = &new_plate;
				info.new_plate_backup = new_plate_backup;
				info.packing_case = 6;
			}

			return info;
		}

		else if (plate.left_wall >= plate.right_wall)
		{
			// intermediate square_size: does not fill width, and is not extension of prev plate

			auto plate_backup = plate;
			plate.exit_heap();
			auto &new_plate = plates[plate.x0 + item.width + 1];
			auto new_plate_backup = new_plate;
			new_plate.prev_x0 = plate.x0;
			new_plate.width = plate.width - item.width;
			new_plate.height = plate.height;
			new_plate.next().prev_x0 = new_plate.x0;
			plate.height += item.height;
			plate.width = item.width;
			new_plate.enter_heap();

			if (prev.is_valley())
			{
				prev.enter_heap();
			}

			info.new_plate = &new_plate;
			info.new_plate_backup = new_plate_backup;
			info.plate_backup = plate_backup;
			info.packing_case = 3;

			return info;
		}

		else
		{
			info.packed_x = plate.x0 + plate.width - item.width;

			plate.exit_heap();
			auto &new_plate = plates[plate.x0 + plate.width - item.width + 1];
			auto new_plate_backup = new_plate;
			plate.width -= item.width;
			new_plate.prev_x0 = plate.x0;
			new_plate.width = item.width;
			new_plate.height = plate.height + item.height;
			new_plate.next().prev_x0 = new_plate.x0;
			plate.enter_heap();
			if (next.is_valley())
			{
				next.enter_heap();
			}

			info.new_plate = &new_plate;
			info.new_plate_backup = new_plate_backup;
			info.packing_case = 7;

			return info;
		}

		std::cout << "code should never reach here" << std::endl;
		exit(1);
	}

	/**
	 * @brief Unpacks `item` using the UnpackInformation (which specifies
	 * the plate where the item is, and other things. this information is returned
	 * by `pack_one` when it is used to pack an item). Updates the heap and plates
	 *
	 */
	void unpack_one(std::vector<Plate> &plates,
					RandomHeap<int64_t> &heap,
					std::pair<int64_t, int64_t> container_size,
					Item item,
					const UnpackInformation &information)
	{

		int64_t packing_case = information.packing_case;
		Plate &plate = *information.plate;
		Plate &prev = *information.prev;
		Plate &next = *information.next;

		if (packing_case == 0)
		{
			int64_t prev_diff = information.prev_diff;
			int64_t next_diff = information.next_diff;

			if (prev_diff < 0 && next_diff < 0)
			{
				plate.exit_heap();
			}
			else if (prev_diff > 0 && next_diff > 0)
			{
				if (prev.is_valley())
				{
					prev.exit_heap();
				}

				if (next.is_valley())
				{
					next.exit_heap();
				}
			}
			else if (prev_diff > 0) // therefore next_diff <= 0
			{
				if (next_diff == 0)
				{
					plate.next().prev_x0 = next.x0;
					plate.width -= next.width;
				}

				if (prev.is_valley())
				{
					prev.exit_heap();
				}
			}
			else if (next_diff > 0) // therefore prev_diff <= 0
			{
				if (prev_diff == 0)
				{
					plate.next().prev_x0 = plate.x0;
					prev.width -= plate.width;
				}

				if (next.is_valley())
				{
					next.exit_heap();
				}
			}
			else if (prev_diff < 0) // and next_diff == 0
			{
				if (plate.is_valley())
				{
					plate.exit_heap();
				}

				plate.next().prev_x0 = next.x0;
				plate.width -= next.width;
			}
			else if (next_diff < 0) // and prev_diff == 0
			{
				if (prev.is_valley())
				{
					prev.exit_heap();
				}

				next.prev_x0 = plate.x0;
				prev.width -= plate.width;
			}
			else // both are 0
			{

				if (prev.is_valley())
				{
					prev.exit_heap();
				}

				prev.next().prev_x0 = next.x0;
				prev.width -= plate.width + next.width;
			}

			plate.height -= item.height;
			plate.enter_heap();
		}
		else if (packing_case == 1)
		{
			Plate &new_plate = *information.new_plate;
			Plate new_plate_backup = information.new_plate_backup;

			new_plate.exit_heap();
			next.prev_x0 = plate.x0;
			new_plate = new_plate_backup;
			prev.width -= item.width;
			plate.enter_heap();
		}
		else if (packing_case == 2)
		{
			Plate plate_backup = information.plate_backup;
			Plate &new_plate = *information.new_plate;
			Plate new_plate_backup = information.new_plate_backup;

			new_plate.exit_heap();
			plate = plate_backup;
			new_plate.next().prev_x0 = plate.x0;
			new_plate = new_plate_backup;
			plate.enter_heap();
		}
		else if (packing_case == 3)
		{
			Plate plate_backup = information.plate_backup;
			Plate &new_plate = *information.new_plate;
			Plate new_plate_backup = information.new_plate_backup;

			if (prev.is_valley())
			{
				prev.exit_heap();
			}
			new_plate.exit_heap();
			plate = plate_backup;
			new_plate.next().prev_x0 = plate.x0;
			new_plate = new_plate_backup;
			plate.enter_heap();
		}
		else if (packing_case == 4)
		{
			int64_t prev_diff = information.prev_diff;
			int64_t next_diff = information.next_diff;
			int64_t height_diff = information.height_diff;

			if (prev_diff == 0 && next_diff == 0)
			{
				if (prev.is_valley())
				{
					prev.exit_heap();
				}

				prev.next().prev_x0 = next.x0;
				prev.width -= plate.width + next.width;
			}
			else if (prev_diff == 0 && next_diff < 0)
			{
				if (prev.is_valley())
				{
					prev.exit_heap();
				}

				prev.next().prev_x0 = plate.x0;
				prev.width -= plate.width;
			}
			else if (next_diff == 0 and prev_diff < 0)
			{
				if (plate.is_valley())
				{
					plate.exit_heap();
				}

				plate.next().prev_x0 = next.x0;
				plate.width -= next.width;
			}
			else
			{
				plate.exit_heap();
			}

			plate.height -= height_diff;
			plate.enter_heap();
		}
		else if (packing_case == 5)
		{
			auto &old_next = *information.old_next;
			auto &new_plate = *information.new_plate;
			auto &new_plate_backup = information.new_plate_backup;

			plate.exit_heap();
			old_next.next().prev_x0 = old_next.x0;
			new_plate = new_plate_backup;
			plate.width += item.width;
			plate.enter_heap();
		}
		else if (packing_case == 6)
		{
			auto &new_plate = *information.new_plate;
			auto &new_plate_backup = information.new_plate_backup;

			plate.exit_heap();
			new_plate.next().prev_x0 = plate.x0;
			new_plate = new_plate_backup;
			plate.width += item.width;
			plate.enter_heap();
		}
		else
		{
			auto &new_plate = *information.new_plate;
			auto &new_plate_backup = information.new_plate_backup;

			if (next.is_valley())
			{
				next.exit_heap();
			}
			plate.exit_heap();
			new_plate = new_plate_backup;
			plate.width += item.width;
			next.prev_x0 = plate.x0;
			plate.enter_heap();
		}
	}

	/**
	 * @brief Sets up the plates and heap, and implicitly returns them via the pointer arguments
	 *
	 *
	 * @details
	 * 	Sets up three plates: two helper plates outside the container (width=1, height=container height + 1) and one
	 * 	corresponding to the container floor (width=width of container, height = 0). See diagram:
	 *	                         1
	 *	                        ◄───►
	 *
	 *	    ▲  ─────┐           ┌────
	 *	    │       ├───────────┤
	 *	    │       │           │
	 *	H+1 │       │           │
	 *	    │       │           │
	 *	    │       │           │
	 *	    │       │           │
	 *	    │       │           │
	 *	    ▼       └───────────┘
	 *
	 *	            ◄───────────►
	 *	                 W
	 *
	 */
	void create_initial_configuration(
		std::pair<int64_t, int64_t> container_size,
		RandomHeap<int64_t> *heap_out,
		std::vector<Plate> *plates_out)
	{
		std::vector<Plate> plates(container_size.first + 2, Plate(plates_out, heap_out, -1, -1, -1, -1, container_size));
		plates[0] = Plate(plates_out, heap_out, 1, container_size.second + 1, -1, -1, container_size);
		plates[1] = Plate(plates_out, heap_out, container_size.first, 0, 0, -1, container_size);
		plates[container_size.first + 1] = Plate(plates_out, heap_out, 1, container_size.second + 1, container_size.first, 0, container_size);
		for (int64_t i = 0; i < plates.size(); i++)
		{
			plates[i].x0 = i - 1;
			plates[i].heap_entry.setKey(i - 1);
		}

		*plates_out = std::move(plates);

		(*plates_out)[1].enter_heap();
	}

}