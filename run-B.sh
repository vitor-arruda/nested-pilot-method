# Runs IterativeNesting-H0/H1/H2 for 10s and 60s on gcutB08-gcutB13, and on cgcutB03
# gcutB comes from IDBS material (by rotating items), and cgcutB comes from Rakotonirainy data source

g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 -rotate-items | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 -rotate-items | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 -rotate-items | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 -rotate-items | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 -rotate-items | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/IDBS/gcut08.pack datasets/IDBS/gcut09.pack datasets/IDBS/gcut10.pack datasets/IDBS/gcut11.pack datasets/IDBS/gcut12.pack datasets/IDBS/gcut13.pack
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 -rotate-items | ./relog.sh 
done


g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/Rakotonirainy/CGCUT/1977ChristofidesWhitlock3.csv
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done