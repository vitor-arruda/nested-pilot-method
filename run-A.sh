# Runs IterativeNesting-H0/H1/H2 for 10s and 60s on all instances (version A of gcut and cgcut)
# Almost all input files come from IDBS material, except for ZDF which comes from Rakotonirainy

g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 10 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2
for i in datasets/IDBS/* datasets/Rakotonirainy/ZDF/* 
do
   ./main.out -mode NestedPilot -input-file $i -time-limit 60 -sleep 0 | ./relog.sh 
done