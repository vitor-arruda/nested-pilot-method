set ITEMS;

# widths, heights and quantities of items
param w {ITEMS};
param h {ITEMS};
param q {ITEMS};

# container width and height
param cW;
param cH;

# waste upper bound
## This is set to the best solution we know of (because the
## purpose of this program is to test that the best known solution
## is optimal)
param waste_bound;

# necessary items
## These are items that must be present, otherwise their absence will
## already be enough to violate the waste upper bound (above).
##
## For example: suppose the total area of the items is 20, the area
## of the container is 24 (so even if there is a solution that packs all items,
## it will still have a trim loss of 4), the waste upper bound is 10 and 
## there is a 2x3 item `i` with 3 copies (i.e. there are 3 items with these 
## dimensions).
## 
## Then, if 2 of these 3 copies are missing in a candidate solution, then
## the waste of this solution is at least `(24 - 20) + 2x(2x3) = 16` which
## violates the upper bound (10), hence this solution is infeasible. 
## In this example, any feasible solution must contain at least 2 copies of
## this item (i.e. at most 1 copy may be missing).
## 
## We manually calculated the minimum number of copies for each item
param necessary_items {ITEMS};

# the parameters called "a_ipqrs" in J. E. Beasley, (1985) An Exact Two-Dimensional Non-Guillotine 
# Cutting Tree Search Procedure. Operations Research 33(1):49-64. http://dx.doi.org/10.1287/opre.33.1.49
param g {i in ITEMS, x in 0..cW-1, y in 0..cH-1, u in 0..cW-1, v in 0..cH-1} = 
	if  x <= u <= x + w[i] - 1 and y <= v <= y + h[i] - 1 then 1 else 0;

# a[i, x, y] is 1 if, and only if, a copy of item `i` is packed with its lower-left corner at coordinates (x, y)
var a {i in ITEMS, x in 0..cW-1, y in 0..cH-1} binary;

minimize waste: cW * cH - sum{i in ITEMS} sum{x in 0..cW-1} sum{y in 0..cH-1} a[i, x, y] * w[i] * h[i];

# no two items overlap
subject to non_overlap{u in 0..cW-1, v in 0..cH-1}: sum{i in ITEMS} sum{x in 0..cW-1} sum{y in 0..cH-1} 
	g[i, x, y, u, v] * a[i, x, y] <= 1;
	
# all items are contained inside the container bounds
subject to inside_container{i in ITEMS, x in 0..cW-1, y in 0..cH-1: x > cW - w[i] or y > cH - h[i]}: a[i, x, y] = 0;

# no item appears more times in the solution than there are available copies of it
subject to limit_quantity{i in ITEMS}: sum{x in 0..cW-1} sum{y in 0..cH-1} a[i, x, y] <= q[i];

# the solution has waste <= waste upper bound
subject to waste_upper_bound: cW * cH - sum{i in ITEMS} sum{x in 0..cW-1} sum{y in 0..cH-1} a[i, x, y] * w[i] * h[i] 
	<= waste_bound;

# refer to the explanation of parameter `necessary_items`
subject to must_be_present{i in ITEMS}: sum{x in 0..cW-1} sum{y in 0..cH-1} a[i, x, y] >= necessary_items[i];

# the packing layout is left-justified (i.e. no item can be translated to the left)
subject to left_justified_layout{i in ITEMS, x in 1..cW-1, y in 0..cH-1}: 
	sum{j in ITEMS: w[j] <= x} 
		sum{v in 0..cH-1 : v + h[j] > y and v < y + h[i]} 
			a[j, x - w[j], v] 
	>= a[i, x, y];

# the packing layout is bottom-justified (i.e. no item can be translated down)
subject to bottom_justified_layout{i in ITEMS, x in 0..cW-1, y in 1..cH-1}: 
	sum{j in ITEMS: h[j] <= y} 
		sum{u in 0..cW-1 : u + w[j] > x and u < x + w[i]} 
			a[j, u, y - h[j]] 
	>= a[i, x, y];

# some item is packed at the origin (bottom-left corner on the bottom-left corner of the container)
subject to some_item_at_origin: sum{i in ITEMS} a[i, 0, 0] = 1;