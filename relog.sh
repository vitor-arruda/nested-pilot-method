#!/bin/bash

# Utility to prepend the current time to the output
# of another process. 
# Example usage:
#       echo "hello, this is a test message" | ./relog.sh
#       [Wed Mar 23 10:23:53 -04 2022] hello, this is a test message

while read -r line; do
        timestamp=`date`
        echo "[$timestamp] $line"
done