# Examples of how to compile the IterativeNesting source code

# g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H0
g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H1
# g++ -O3 src/main.cpp -o main.out -gdwarf -std=c++17 -DUSE_HEURISTIC_H2